import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class dataStructure {
    public static void main(String[] args) {
        Map<Integer, Map<String, String>> map = new HashMap<Integer, Map<String, String>>();
        String line = "";

        File file = new File("csvFiles/deliveries.csv");

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String[] columns = br.readLine().split(",");
            int count = 1;

            while((line = br.readLine()) != null) {
                Map<String, String> row = new HashMap<String, String>();
                int columnCount = 0;
                String[] data = line.split(",");
                int i = 0;
                while(i < data.length) {
                    row.put(columns[columnCount], data[columnCount]);
                    columnCount++;
                    i++;
                }
                map.put(count, row);
                count++;
            }
            br.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }






        Map<Integer, Map<String, String>> map1 = new HashMap<Integer, Map<String, String>>();
        String line1 = "";

        File file1 = new File("csvFiles/matches.csv");

        try {
            BufferedReader br = new BufferedReader(new FileReader(file1));
            String[] columns1 = br.readLine().split(",");
            int count1 = 1;

            while((line1 = br.readLine()) != null) {
                Map<String, String> row1 = new HashMap<String, String>();
                int columnCount1 = 0;
                String[] data1 = line1.split(",");
                int i = 0;
                while(i < data1.length) {
                    row1.put(columns1[columnCount1], data1[columnCount1]);
                    columnCount1++;
                    i++;
                }
                map1.put(count1, row1);
                count1++;
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        //printTable(map1);


        //question1(map1);

        //question2(map1);
        //question3(map2);
    }
    public static void printTable(Map<Integer, Map<String, String>> map) {
        for(int temp: map.keySet()) {
            String s = "";
            s += temp + "   ";

            for(String value: map.get(temp).keySet()) {
                s += map.get(temp).get(value) + "  ";
            }
            System.out.println(s);
        }
    }

    public static void question1(Map<Integer, Map<String, String>> map) {
        Map<Integer, Integer> sol = new HashMap<Integer, Integer>();

        for(int temp: map.keySet()) {
            Integer num = Integer.parseInt(map.get(temp).get("season"));
            if(sol.containsKey(num)) {
                sol.put(num, sol.get(num) + 1);
            }
            else {
                sol.put(num, 1);
            }
        }

        for(int temp: sol.keySet()) {
            System.out.println(temp + "  " + sol.get(temp));
        }
    }

    public static void question2(Map<Integer, Map<String, String>> map) {
        Map<String, Integer> sol = new HashMap<String, Integer>();
        for(int temp: map.keySet()) {
            String num = map.get(temp).get("winner");
            if(sol.containsKey(num)) {
                sol.put(num, sol.get(num) + 1);
            }
            else {
                sol.put(num, 1);
            }
        }

        for(String temp: sol.keySet()) {
            System.out.println(temp + "  " + sol.get(temp));
        }
    }


}
